INTRO

Situated in the center of Bucharest near the cross between Bulevardul Dacia and Calea Grivitei, LABORATOR PROPRIU will soon open as a multifunctional space that will incorporate a restaurant, a caf�, a wine bar, a fashion store, a deco showroom and an art gallery.
In the recent years Bucharest�s day-life adopted rather the American Mall culture neglecting European trends that favored the development of little shops and cafes in the center of the cities. 
We believe that there�s a great need among a part of Bucharest�s citizens to counterbalance this evolution and enjoy a stylish but yet affordable living in a space that represents them through its design, quality of service and offer of products.

CREDO

We strongly believe that our efforts will contribute to a better society and a development of a hard to achieve �normality�.
LABORATOR PROPRIU will establish itself as a trend-setting place that has the capacity of permanently re-inventing its image, product offer and food menu.
One of our main goals is to always surprise our customers with innovative concepts that we will integrate in all the different areas of our multifunctional space. A constant metamorphosis of this space will be one of our defining trademarks.
LABORATOR PROPRIU will be from the very start extremely open to all kind of collaborations and creative initiatives. 
To be unique and offer gastronomic, deco and fashion products that you�re hardly able to find somewhere else is one of our main goals too.

SPACE

LABORATOR PROPRIU is located on Calea Grivitei 41. 

The main part of the space, that also faces the street with its big windows, is a beautiful house build in the 19th century that belongs to Bucharest�s cultural patrimony. In the backside of this house there are 3 other spaces that have been constructed more recently in the 20th century. At the very end of the property there�s a 150 square meter big garden that will host every year from March till October the cozy and green terrace of LABORATOR PROPRIU.

This more then 500 square meter big space will host in its labyrinthic interior a
- restaurant
- le lacto bar
- wine bar
- fashion store
- deco showroom
- art gallery
All areas will be connected one to each other in a very fluid way, offering the visitors multiple possibilities of spending their time in the same space. The conceptual structure of LABORATOR PROPRIU will allow its customers to easily combine gastronomic pleasures, shopping and re-creative activities.
