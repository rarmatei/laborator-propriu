Le Lacto Bar

This will soon be your favorite place to enjoy a tasty breakfast out in town, first of all because there aren�t almost any in Bucharest offering a good breakfast.
 It is our mission to celebrate a revival of the classic Romanian Lacto Bar and reinvent it, bringing it through its stylish design and offer of quality food and drinks into the 21st century.
�FRESH� will be the main motto of LABORATOR PROPRIU�S Lacto Bar.
Here you�ll enjoy a large variety of omlets and eggs cooked in any possible form. Crispy toasted bread, farm butter, lots of homemade gems and freshly squeezed fruit juices will complete the menu.
If you prefer a more simple breakfast with just coffee and a croissant, you�ll be well served too in our Lacto Bar.
