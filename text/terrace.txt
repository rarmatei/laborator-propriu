The Terrace

With the summer just kicking off, we are so pleased to be able to offer our customers a spacious but meantime green and cozy terrace.
It has a total surface of over 150 square meters and will be for sure the main attraction during the summer months. 
LABORATOR PROPRIUíS terrace will not only be a place to consume food and drinks, but it will also transform from time to time into an open cinema.
